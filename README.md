Business Name: Leah Legal Criminal Defense

Address: 1020 E. 59th St., Los Angeles, CA 90001 USA

Phone: (213) 444-7818

Website: https://www.leahlegal.com

Description: Leah Naparstek practices exclusively in the area of criminal defense throughout California. She is an aggressive and talented attorney who represents individuals accused of crimes ranging from minor misdemeanors to serious felonies. She is highly knowledgeable in the area of DUI defense and obtains outstanding results in this specialized area of law.

Keywords: Criminal Defense Attorney, DUI defense, Criminal justice attorney.

Hour: 24/7.

Year: 2014.
